# Laptop Service
import os
import flask
from flask import Flask
from flask_restful import Resource, Api
from pymongo import MongoClient
# Instantiate the app
app = Flask(__name__)
api = Api(app)


client = MongoClient('db', 27017)
db = client.tododb


class Brevet(Resource):
    def get(self, cases = 'listAll', dis_format = 'json'):
        _items = db.tododb.find()
        items = [item for item in _items]
        for item in items:
            item.pop("_id")
        app.logger.debug(items)
        if dis_format == 'json':
            if cases == 'listAll':
                return items
            elif cases == 'listOpenOnly':
                open_only = items
                for item in open_only:
                    item.pop(close_time)
                return open_only
            elif cases == 'listClose':
                close_only = items
                for item in open_only:
                    item.pop(open_time)
                return close_only
            else:
                abort(404, message="{} doesn't exist".format(cases))
        elif dis_format == 'csv':
            if cases == 'listAll':
                csv_all = []
                for i in len(items):
                    csv_all[i] = items[i]["km"] + " , " + items[i]["open_time"] + " , " + items[i]["close_time"]
                return csv_all
            elif cases == 'listOpenOnly':
                csv_open_only = []
                for i in len(items):
                    csv_open_only[i] = items[i]["km"] + " , " + items[i]["open_time"]
                return csv_open_only
            elif cases == 'listClose':
                csv_close_only = []
                for i in len(items):
                    csv_close_only[i] = items[i]["km"] + " , " + items[i]["close_time"]
                return csv_close_only
            else:
                abort(404, message="{} doesn't exist".format(cases))
        else:
      	    abort(404, message="Something went wrong")



# api.add_resource(Brevet, '/')
api.add_resource(Brevet, '/')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
